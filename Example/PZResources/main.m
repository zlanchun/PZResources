//
//  main.m
//  PZResources
//
//  Created by zhenglanchun on 05/25/2017.
//  Copyright (c) 2017 zhenglanchun. All rights reserved.
//

@import UIKit;
#import "PZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PZAppDelegate class]));
    }
}
