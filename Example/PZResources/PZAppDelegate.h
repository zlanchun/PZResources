//
//  PZAppDelegate.h
//  PZResources
//
//  Created by zhenglanchun on 05/25/2017.
//  Copyright (c) 2017 zhenglanchun. All rights reserved.
//

@import UIKit;

@interface PZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
