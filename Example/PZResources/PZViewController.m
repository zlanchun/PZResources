//
//  PZViewController.m
//  PZResources
//
//  Created by zhenglanchun on 05/25/2017.
//  Copyright (c) 2017 zhenglanchun. All rights reserved.
//

#import "PZViewController.h"
#import "UIImage+PZImageLoader.h"
#import "UIFont+PZFontLoader.h"

@interface PZViewController ()
@property (weak, nonatomic) IBOutlet UILabel *iconfont;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation PZViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.iconfont.font = [UIFont iconFontOfSize:24];
    self.iconfont.text = @"Download \U0000e607";
    
    self.imageView.image = [UIImage pz_imageNamed:@"tanner-vines-44136.jpg" ofType:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
