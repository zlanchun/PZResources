//
//  UIImage+PZImageLoader.m
//  Pods
//
//  Created by z on 2017/5/25.
//
//

#import "UIImage+PZImageLoader.h"

NS_ASSUME_NONNULL_BEGIN
@implementation UIImage (PZImageLoader)
+ (UIImage *)pz_imageNamed:(NSString *)name ofType:(nullable NSString *)type {
    NSString *mainBundlePath = [NSBundle mainBundle].bundlePath;
    NSString *bundlePath = [NSString stringWithFormat:@"%@/%@",mainBundlePath,@"PZResources.bundle"];
    NSBundle *bundle = [NSBundle bundleWithPath:bundlePath];
    if (bundle == nil) {
        bundlePath = [NSString stringWithFormat:@"%@/%@",mainBundlePath,@"Frameworks/PZResources.framework/PZResources.bundle"];
        bundle = [NSBundle bundleWithPath:bundlePath];
    }
    if ([UIImage respondsToSelector:@selector(imageNamed:inBundle:compatibleWithTraitCollection:)]) {
        return [UIImage imageNamed:name inBundle:bundle compatibleWithTraitCollection:nil];
    } else {
        return [UIImage imageWithContentsOfFile:[bundle pathForResource:name ofType:type]];
    }
}
@end
NS_ASSUME_NONNULL_END
