//
//  UIImage+PZImageLoader.h
//  Pods
//
//  Created by z on 2017/5/25.
//
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface UIImage (PZImageLoader)
+ (UIImage *)pz_imageNamed:(NSString *)name ofType:(nullable NSString *)type;
@end
NS_ASSUME_NONNULL_END
