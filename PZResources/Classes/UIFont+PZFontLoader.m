//
//  UIFont+PZFontLoader.m
//  Pods
//
//  Created by z on 2017/5/25.
//
//

#import "UIFont+PZFontLoader.h"
#import <CoreText/CTFontManager.h>

/**
 * change to your iconfont's fontName
 */
#define PZFontLoaderFontName @"unsplashFont"

@implementation UIFont (PZFontLoader)

+ (UIFont *)iconFontOfSize:(CGFloat)size
{
    UIFont *font = [UIFont fontWithName:PZFontLoaderFontName size:size];
    if (!font) {
        [[self class] dynamicallyLoadFontNamed:PZFontLoaderFontName];
        font = [UIFont fontWithName:PZFontLoaderFontName size:size];
        if (!font) font = [UIFont systemFontOfSize:size];
    }
    return font;
}

+ (void)dynamicallyLoadFontNamed:(NSString *)name
{
    NSString *fontfileName = @"iconfont.ttf";
    
    NSString *mainBundlePath = [NSBundle mainBundle].bundlePath;
    NSString *bundlePath = [NSString stringWithFormat:@"%@/%@",mainBundlePath,@"PZResources.bundle"];
    NSBundle *bundle = [NSBundle bundleWithPath:bundlePath];
    if (bundle == nil) {
        bundlePath = [NSString stringWithFormat:@"%@/%@",mainBundlePath,@"Frameworks/PZResources.framework/PZResources.bundle"];
        bundle = [NSBundle bundleWithPath:bundlePath];
    }
    NSString *resourcePath = [NSString stringWithFormat:@"%@/%@",bundle.bundlePath,fontfileName];
    
    NSURL *url = [NSURL fileURLWithPath:resourcePath];
    
    NSData *fontData = [NSData dataWithContentsOfURL:url];
    if (fontData) {
        CFErrorRef error;
        CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)fontData);
        CGFontRef font = CGFontCreateWithDataProvider(provider);
        if (! CTFontManagerRegisterGraphicsFont(font, &error)) {
            CFStringRef errorDescription = CFErrorCopyDescription(error);
            NSLog(@"Failed to load font: %@", errorDescription);
            CFRelease(errorDescription);
        }
        CFRelease(font);
        CFRelease(provider);
    }
}

@end
