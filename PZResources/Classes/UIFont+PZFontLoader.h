//
//  UIFont+PZFontLoader.h
//  Pods
//
//  Created by z on 2017/5/25.
//
//

#import <UIKit/UIKit.h>

@interface UIFont (PZFontLoader)
+ (UIFont *)iconFontOfSize:(CGFloat)size;
@end
