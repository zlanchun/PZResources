# PZResources

[![CI Status](http://img.shields.io/travis/zhenglanchun/PZResources.svg?style=flat)](https://travis-ci.org/zhenglanchun/PZResources)
[![Version](https://img.shields.io/cocoapods/v/PZResources.svg?style=flat)](http://cocoapods.org/pods/PZResources)
[![License](https://img.shields.io/cocoapods/l/PZResources.svg?style=flat)](http://cocoapods.org/pods/PZResources)
[![Platform](https://img.shields.io/cocoapods/p/PZResources.svg?style=flat)](http://cocoapods.org/pods/PZResources)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PZResources is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "PZResources"
```

## Author

zhenglanchun, zlanchun@icloud.com

## License

PZResources is available under the MIT license. See the LICENSE file for more info.
